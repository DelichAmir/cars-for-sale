from bs4 import BeautifulSoup
import requests
import lxml
import pandas as pd

# The base page with info
base_url = "https://losangeles.craigslist.org/search/sss?query=cars&sort=rel"

# Send Get http request
page = requests.get(base_url)

# Werify we had a successful Get request webpage call 
if page.status_code == requests.codes.ok:
  # Get the whole webpage in beautiful Soup format
  bs = BeautifulSoup(page.text, 'lxml')

list_cars = bs.find('div', class_='content').find('ul', class_='rows' ).find_all('li')


# Data of cars 
data = {
  'Name' : [],
  'Price' : [],
  'Date' : []
}

# Finding information about car
for info in list_cars:
  # Get the name of the item info
  name = info.find('a', class_='result-title hdrlnk').text
  if name:
    data['Name'].append(name)
  else:
    data['Name'].append('None')
  
  # Get the  price of the car
  price = info.find("span" , class_='result-price').text
  if price:
    data['Price'].append(price)
  else:
    data['Price'].append('None')
  
  # Get  the year of published of the item
  date = info.find('time')['title']
  if date:
    data['Date'].append(date)
  else:
    data['Date'].append('None')



# Making data in a table form
table = pd.DataFrame(data, columns = ['Date','Price','Name'])
# Making raws start with " 1 " 
table.index = table.index + 1
# Save the table to csv file
table.to_csv('Cars_sale',sep = ',',index = False, encoding = 'UTF-8')

